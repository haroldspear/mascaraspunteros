#include <stdio.h>
#include "mask.h"


static int mask_owner, mask_group, mask_other;

int main() {			
	char input;
	mask_owner = mask_group = mask_other = 0;

    	printf("Permiso lectura para owner? [s/n]"); //s
	input = getchar();
	getchar();//this is to catch the /n
	setPermiso(&mask_owner,'r',(input=='s'? SET:UNSET ));	

    	printf("Permiso escritura para owner? [s/n]"); //s
	input = getchar();
	getchar();
	setPermiso(&mask_owner,'w',(input=='s'? SET:UNSET ));	

    	printf("Permiso ejecución para owner? [s/n]"); //n
	input = getchar();
	getchar();
	setPermiso(&mask_owner,'x',(input=='s'? SET:UNSET ));

	
    	printf("Permiso lectura para group? [s/n]"); //s
	input = getchar();
	getchar();
	setPermiso(&mask_group,'r',(input=='s'? SET:UNSET ));

    	printf("Permiso escritura para group? [s/n]"); //s
	input = getchar();
	getchar();
	setPermiso(&mask_group,'w',(input=='s'? SET:UNSET ));

    	printf("Permiso ejecución para group? [s/n]"); //n
	input = getchar();
	getchar();
	setPermiso(&mask_group,'x',(input=='s'? SET:UNSET ));

	
    	printf("Permiso lectura para other? [s/n]"); //s
	input = getchar();
	getchar();
	setPermiso(&mask_other,'r',(input=='s'? SET:UNSET ));
    	
	printf("Permiso escritura para other? [s/n]"); //n
	input = getchar();
	getchar();
	setPermiso(&mask_other,'w',(input=='s'? SET:UNSET ));

    	printf("Permiso ejecución para other? [s/n]"); //n
	input = getchar();
	getchar();
	setPermiso(&mask_other,'x',(input=='s'? SET:UNSET ));
	

	printf("Permisos asignados: %d%d%d \n",mask_owner,mask_group,mask_other);

}

